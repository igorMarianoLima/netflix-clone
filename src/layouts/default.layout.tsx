import { Header } from "../components/Header/Header";

export const DefaultLayout: React.FC<React.PropsWithChildren> = ({
  children,
}) => {
  return (
    <>
      <Header />
      <main>{children}</main>
      <h2>Footer</h2>
    </>
  );
};
