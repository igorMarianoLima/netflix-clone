import { createGlobalStyle } from "styled-components";

export const GlobalStyle = createGlobalStyle`
    * {
        margin: 0;
        padding: 0;

        box-sizing: border-box;

        list-style: none;
        text-decoration: none;

        font-family: "Netflix Sans", sans-serif;

        border: 0;
        outline: 0;
    }

    body {
        min-height: 100vh;
        background-color: #141414;

        color: #fff;
    }
`;
