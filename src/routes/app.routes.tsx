import {
  createBrowserRouter,
  createRoutesFromElements,
  Route,
  RouterProvider,
  Outlet,
} from "react-router-dom";

import HomePage from "../pages/Home/home.page";
import LoginPage from "../pages/Login/login.page";

import NotFoundPage from "../pages/NotFound/notFound.page";

export const routes = createBrowserRouter(
  createRoutesFromElements(
    <>
      <Route path="/" element={<Outlet />}>
        <Route path="/" element={<HomePage />} />
        <Route path="/login" element={<LoginPage />} />
      </Route>

      <Route path="*" element={<NotFoundPage />} />
    </>
  )
);

export const Router = () => <RouterProvider router={routes} />;
