import "./styles/Fonts.css";
import { GlobalStyle } from "./styles/GlobalStyle";

import { Router as AppRouter } from "./routes/app.routes";

const App = () => {
  return (
    <>
      <GlobalStyle />
      <AppRouter />
    </>
  );
};

export default App;
