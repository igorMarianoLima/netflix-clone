import styled from "styled-components";
import { ButtonVariant } from "./Button";

interface Button {
  variant: ButtonVariant;
}

const background: Record<ButtonVariant, string> = {
  ghost: "transparent",
  primary: "#fff",
};

export const Button = styled.button.attrs({
  type: "button",
})<Button>`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-wrap: wrap;
  gap: 12px;

  width: fit-content;

  appearance: none;
  cursor: pointer;

  background-color: ${(props) => background[props.variant]};
`;

export const IconWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;

  width: 24px;
  height: 24px;
`;
