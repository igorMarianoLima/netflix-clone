import { ReactElement } from "react";

import * as S from "./Button.styles";

export type ButtonVariant = "ghost" | "primary";

interface ButtonProps extends React.HTMLAttributes<HTMLButtonElement> {
  icon?: ReactElement;
  variant?: "ghost" | "primary";
}

export const Button: React.FC<React.PropsWithChildren<ButtonProps>> = ({
  icon,
  variant = "primary",
  children,
  ...props
}) => {
  return (
    <S.Button variant={variant} {...props}>
      {children}

      {icon && <S.IconWrapper>{icon}</S.IconWrapper>}
    </S.Button>
  );
};
