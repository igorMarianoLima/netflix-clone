import { Link } from "react-router-dom";

import { Button } from "../Button/Button";

import { Logo } from "../svgs/Logo";
import { Search } from "../svgs/Search";
import * as S from "./Header.styles";

export const Header: React.FC = () => {
  const links = [
    {
      label: "Home",
      link: "/",
    },
    {
      label: "TV Shows",
      link: "/",
    },
    {
      label: "Movies",
      link: "/",
    },
    {
      label: "New & Popular",
      link: "/",
    },
    {
      label: "My List",
      link: "/",
    },
    {
      label: "Browse by Languages",
      link: "/",
    },
  ];

  const buttons = [
    {
      label: "Search",
      icon: <Search />,
      onClick: () => console.log("search"),
    },
  ];

  return (
    <S.Header>
      <S.Navigator>
        <S.NavigatorWrapper>
          <Link to="/">
            <S.LogoWrapper>
              <Logo />
            </S.LogoWrapper>
          </Link>

          <S.List>
            {links.map((link) => (
              <li key={link.label}>
                <Link to={link.link}>{link.label}</Link>
              </li>
            ))}
          </S.List>
        </S.NavigatorWrapper>

        <S.NavigatorWrapper>
          <S.List>
            {buttons.map((button) => (
              <li key={button.label}>
                <Button
                  variant="ghost"
                  icon={button.icon}
                  onClick={button.onClick}
                  title={button.label}
                />
              </li>
            ))}
          </S.List>
        </S.NavigatorWrapper>
      </S.Navigator>
    </S.Header>
  );
};
