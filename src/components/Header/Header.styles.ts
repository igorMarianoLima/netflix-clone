import styled from "styled-components";

export const Header = styled.header`
  display: flex;
  align-items: center;
  justify-content: center;

  background-color: #141414;

  width: 100%;
  height: 82px;
`;

export const Navigator = styled.nav`
  display: flex;
  align-items: center;
  justify-content: space-between;

  width: 100%;
  max-width: 1320px;
`;

export const NavigatorWrapper = styled.div`
  display: flex;
  align-items: center;
  gap: 50px;
`;

export const LogoWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;

  width: 111px;
`;

export const List = styled.ul`
  display: flex;
  align-items: center;
  gap: 20px;

  a {
    color: #fff;

    font-weight: 500;
    font-size: 14px;
    line-height: 17px;
  }
`;
